package com.byteinfotech.corona.coro;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.byteinfotech.corona.coro.requests.Call;
import com.byteinfotech.corona.coro.requests.Food;
import com.byteinfotech.corona.coro.requests.Medicine;
import com.byteinfotech.corona.coro.requests.Query;
import com.byteinfotech.corona.coro.requests.Transport;
import com.byteinfotech.corona.coro.requests.Volunteer;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {
    BottomNavigationView bottomNavigation;
    CardView food,med,trans,query,call,reg;
    Button vol;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        food=findViewById(R.id.food);
        med=findViewById(R.id.medicine);
        trans=findViewById(R.id.transport);
        query=findViewById(R.id.query);
        call=findViewById(R.id.call);
        reg=findViewById(R.id.et_submit);

        food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, Food.class);
                startActivity(myIntent);
            }
        });
        query.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, Query.class);
                startActivity(myIntent);
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, Call.class);
                startActivity(myIntent);
            }
        });
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, Volunteer.class);
                startActivity(myIntent);
            }
        });
        med.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, Medicine.class);
                startActivity(myIntent);
            }
        });
        trans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, Transport.class);
                startActivity(myIntent);
            }
        });

        bottomNavigation = findViewById(R.id.bottom_navigation);

        Menu menu = bottomNavigation.getMenu();
        MenuItem menuItem = menu.getItem(2);
        menuItem.setChecked(true);

        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        Intent myIntent = new Intent(MainActivity.this, MapActivity.class);
                        startActivity(myIntent);
                        finish();
                        break;
                    case R.id.navigation_sms:
                        Intent myIntent1 = new Intent(MainActivity.this, MainActivity.class);
                        startActivity(myIntent1);
                        finish();
                        break;
                    case R.id.navigation_stat:
                        Intent myIntent2 = new Intent(MainActivity.this, Stat.class);
                        startActivity(myIntent2);
                        finish();
                        break;

                }
                return false;
            }
        });


    }



}
