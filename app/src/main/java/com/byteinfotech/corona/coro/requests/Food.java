package com.byteinfotech.corona.coro.requests;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.byteinfotech.corona.coro.MainActivity;
import com.byteinfotech.corona.coro.R;
import com.google.firebase.firestore.FirebaseFirestore;
import com.skydoves.powerspinner.OnSpinnerItemSelectedListener;
import com.skydoves.powerspinner.PowerSpinnerView;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Food extends AppCompatActivity {

    EditText name,phone,address,latlng,thing;
    Button loc,submit;
    PowerSpinnerView what,how;
    FirebaseFirestore db;
    Map< String, Object > newContact;
    String longi,lati,currentDateandTime,cat,qua;
    String cityName,state,sub;
    private MediaRecorder recorder = null;
    private static String fileName = null;
    AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        builder = new AlertDialog.Builder(this);

        name=findViewById(R.id.et_name);
        phone=findViewById(R.id.et_phone);
        address=findViewById(R.id.address);
        latlng=findViewById(R.id.et_latlng);
        thing=findViewById(R.id.thing);
        String first = "Name";
        String second = "Phone Number";
        String third = "Address";
        String next = "<font color='#EE0000'>*</font>";
        name.setHint(Html.fromHtml(first + next));
        phone.setHint(Html.fromHtml(second + next));
        address.setHint(Html.fromHtml(third + next));

        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        currentDateandTime = sdf.format(new Date());
        Location location = null;
        if (lm != null) {
            location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
        if (location != null) {

            longi = ""+location.getLongitude();
        }
        if (location != null) {
            lati = ""+location.getLatitude();
        }
        Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = gcd.getFromLocation(location.getLatitude(),
                    location.getLongitude(), 1);
            if (addresses.size() > 0) {
                System.out.println(addresses.get(0).getSubLocality());
                state = addresses.get(0).getAdminArea();
                cityName = addresses.get(0).getLocality();
                sub = addresses.get(0).getSubLocality();
               // Toast.makeText(getApplicationContext(), ""+cityName, Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        loc=findViewById(R.id.et_loc);
        submit=findViewById(R.id.et_submit);

        what=findViewById(R.id.what);
        how=findViewById(R.id.how);
        db = FirebaseFirestore.getInstance();





        loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                latlng.setVisibility(View.VISIBLE);
                latlng.setText(lati+","+longi);

            }
        });
        what.setOnSpinnerItemSelectedListener(new OnSpinnerItemSelectedListener<String>() {
            @Override public void onItemSelected(int position, String item) {
                cat=item;
            }
        });
        how.setOnSpinnerItemSelectedListener(new OnSpinnerItemSelectedListener<String>() {
            @Override public void onItemSelected(int position, String item) {
                qua=item;
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateField()) {
                    newContact = new HashMap<>();

                    newContact.put("name", name.getText().toString());

                    newContact.put("contact", phone.getText().toString());
                    newContact.put("address", address.getText().toString());
                    newContact.put("items", thing.getText().toString());
                    newContact.put("lat", lati);
                    newContact.put("lon", longi);
                    newContact.put("state", state.toUpperCase());
                    newContact.put("district", cityName);
                    newContact.put("locality", sub);
                    newContact.put("date", currentDateandTime);
                    newContact.put("category", cat);
                    newContact.put("quantity", qua);
                    newContact.put("status", "0");
                    db.collection("orders").add(newContact);

                    builder.setMessage("Successfully Updated").setTitle("");

                    //Setting message manually and performing action on button click
                    builder.setMessage("Successfully Updated")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    Intent myIntent1 = new Intent(Food.this, MainActivity.class);
                                    startActivity(myIntent1);
                                    finish();

                                }
                            });

                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    //Setting the title manually
                    alert.setTitle("");
                    alert.show();

                }
            }
        });

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent myIntent1 = new Intent(Food.this, MainActivity.class);
                startActivity(myIntent1);
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public boolean validateField(){

        boolean isValidationSuccessful = true;
        String MobilePattern = "[0-9]{10}";

        if(name.getText().toString().trim().equalsIgnoreCase("")){
            Toast.makeText(Food.this,"Name can't be empty",Toast.LENGTH_SHORT).show();
            isValidationSuccessful=false;

        }

        else if(phone.getText().toString().trim().equalsIgnoreCase("")){
            Toast.makeText(Food.this,"Phone number can't be empty",Toast.LENGTH_SHORT).show();
            isValidationSuccessful=false;

        }
        else if(name.getText().toString().trim().equalsIgnoreCase("")){
            Toast.makeText(Food.this,"Name can't be empty",Toast.LENGTH_SHORT).show();
            isValidationSuccessful=false;

        }
        else if(!phone.getText().toString().matches(MobilePattern)) {

            Toast.makeText(getApplicationContext(), "phone number is not valid", Toast.LENGTH_SHORT).show();
            isValidationSuccessful=false;

        }
        else if(address.getText().toString().trim().equalsIgnoreCase("")){
            Toast.makeText(Food.this,"Address can't be empty",Toast.LENGTH_SHORT).show();
            isValidationSuccessful=false;

        }

        return isValidationSuccessful;
    }
}
