package com.byteinfotech.corona.coro;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.byteinfotech.corona.coro.adapter.ModelListView;
import com.byteinfotech.corona.coro.adapter.MyInterface;
import com.byteinfotech.corona.coro.adapter.RetroAdapter;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class Stat extends AppCompatActivity {

    private ListView listView;
    private RetroAdapter retroAdapter;
    BottomNavigationView bottomNavigation;
    TextView rec,con,act,death,under;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stat);

        listView = findViewById(R.id.list);
        rec = findViewById(R.id.nrecover);
        con = findViewById(R.id.nconfirm);
        act = findViewById(R.id.nactive);
        death = findViewById(R.id.ndeath);
        under = findViewById(R.id.more);
        String htmlString="<u>Watch recovered cases >></u>";
        under.setText(Html.fromHtml(htmlString));
        under.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.vastav.live/recovered.html"));
                startActivity(browserIntent1);
            }
        });
        getJSONResponse();
        bottomNavigation = findViewById(R.id.bottom_navigation);

        Menu menu = bottomNavigation.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);

        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        Intent myIntent = new Intent(Stat.this, MapActivity.class);
                        startActivity(myIntent);
                        finish();
                        break;
                    case R.id.navigation_sms:
                        Intent myIntent1 = new Intent(Stat.this, MainActivity.class);
                        startActivity(myIntent1);
                        finish();
                        break;
                    case R.id.navigation_stat:
                        Intent myIntent2 = new Intent(Stat.this, Stat.class);
                        startActivity(myIntent2);
                        finish();
                        break;

                }
                return false;
            }
        });


    }

    private void getJSONResponse(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MyInterface.JSONURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        MyInterface api = retrofit.create(MyInterface.class);

        Call<String> call = api.getString();

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.i("Responsestring", response.body().toString());
                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Log.i("onSuccess", response.body().toString());

                        String jsonresponse = response.body().toString();
                        writeListView(jsonresponse);

                    } else {
                        Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private void writeListView(String response){

        try {
            //getting the whole json object from the response
            JSONObject obj = new JSONObject(response);


                ArrayList<ModelListView> modelListViewArrayList = new ArrayList<>();
                JSONArray dataArray  = obj.getJSONArray("statewise");

                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject dataobj = dataArray.getJSONObject(i);
                    if (!dataobj.getString("state").equals("Total")){

                        ModelListView modelListView = new ModelListView();


                    //modelListView.setImgURL(dataobj.getString("imgURL"));
                    modelListView.setName(dataobj.getString("state"));
                    modelListView.setActive(dataobj.getString("active"));
                    modelListView.setConfirm(dataobj.getString("confirmed"));
                    modelListView.setDeath(dataobj.getString("deaths"));
                    modelListView.setRecover(dataobj.getString("recovered"));

                    modelListViewArrayList.add(modelListView);
                }else{
                        rec.setText(dataobj.getString("recovered"));
                        con.setText(dataobj.getString("confirmed"));
                        act.setText(dataobj.getString("active"));
                        death.setText(dataobj.getString("deaths"));
                    }

                }

                retroAdapter = new RetroAdapter(this, modelListViewArrayList);
                listView.setAdapter(retroAdapter);

//            }else {
//                Toast.makeText(Stat.this, obj.optString("message")+"", Toast.LENGTH_SHORT).show();
//            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
