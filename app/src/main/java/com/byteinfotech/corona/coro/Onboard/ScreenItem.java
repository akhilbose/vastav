package com.byteinfotech.corona.coro.Onboard;

public class ScreenItem {

    String Title,TIT;
    String ScreenImg;

    public String getTIT() {
        return TIT;
    }

    public void setTIT(String TIT) {
        this.TIT = TIT;
    }

    public ScreenItem(String title, String screenImg, String tit) {
        Title = title;
        ScreenImg = screenImg;
        TIT=tit;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public void setScreenImg(String screenImg) {
        ScreenImg = screenImg;
    }

    public String getTitle() {
        return Title;
    }

    public String getScreenImg() {
        return ScreenImg;
    }
}
