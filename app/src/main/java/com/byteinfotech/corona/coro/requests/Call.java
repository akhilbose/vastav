package com.byteinfotech.corona.coro.requests;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.byteinfotech.corona.coro.MainActivity;
import com.byteinfotech.corona.coro.R;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Call extends AppCompatActivity {
    Button mTimePicker;
    TimePickerDialog.OnTimeSetListener mOnTimeSetListener;
    EditText name,phone,address,latlng,thing,need,where,query;
    Button loc,submit;
    FirebaseFirestore db;
    Map< String, Object > newContact;
    String longi,lati,currentDateandTime,cat,qua,sub,state,cityName;
    int hour;
    AlertDialog.Builder builder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        builder = new AlertDialog.Builder(this);

        mTimePicker = (Button)findViewById(R.id.mTimePicker);
        name=findViewById(R.id.et_name);
        phone=findViewById(R.id.et_phone);
        thing=findViewById(R.id.thing);
        query=findViewById(R.id.address);

        String first = "Name";
        String second = "Phone Number";
        String next = "<font color='#EE0000'>*</font>";
        name.setHint(Html.fromHtml(first + next));
        phone.setHint(Html.fromHtml(second + next));

        submit=findViewById(R.id.et_submit);
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        currentDateandTime = sdf.format(new Date());
        Location location = null;
        if (lm != null) {
            location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
        if (location != null) {

            longi = ""+location.getLongitude();
        }
        if (location != null) {
            lati = ""+location.getLatitude();
        }
        Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = gcd.getFromLocation(location.getLatitude(),
                    location.getLongitude(), 1);
            if (addresses.size() > 0) {
                System.out.println(addresses.get(0).getSubLocality());
                state = addresses.get(0).getAdminArea();
                cityName = addresses.get(0).getLocality();
                sub = addresses.get(0).getSubLocality();
                // Toast.makeText(getApplicationContext(), ""+cityName, Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        db = FirebaseFirestore.getInstance();

        mTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mCalendar =  Calendar.getInstance();
                hour = mCalendar.get(Calendar.HOUR_OF_DAY);
                int minute = mCalendar.get(Calendar.MINUTE);


                TimePickerDialog mTimePickerDialog = new TimePickerDialog(
                        Call.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mOnTimeSetListener,
                        hour,minute,false);

                mTimePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                mTimePickerDialog.show();

            }
        });

        mOnTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourofday, int minute) {
                String mTime = hourofday+":"+minute;
                if(hourofday>=hour) {
                    mTimePicker.setText(mTime);
                }
                else{
                    Toast.makeText(getApplicationContext(), "Select valid time", Toast.LENGTH_SHORT).show();
                    mTimePicker.setText("Select valid time");
                }

            }
        };






        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateField()) {
                    newContact = new HashMap<>();

                    newContact.put("name", name.getText().toString());

                    newContact.put("contact", phone.getText().toString());
                    newContact.put("items", thing.getText().toString());
                    newContact.put("date", currentDateandTime);
                    newContact.put("state", state.toUpperCase());
                    newContact.put("district", cityName);
                    newContact.put("locality", sub);
                    newContact.put("need", query.getText().toString());
                    newContact.put("time", mTimePicker.getText().toString());
                    db.collection("call").add(newContact);

                    builder.setMessage("Successfully Updated").setTitle("");

                    //Setting message manually and performing action on button click
                    builder.setMessage("Successfully Updated")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    Intent myIntent1 = new Intent(Call.this, MainActivity.class);
                                    startActivity(myIntent1);
                                    finish();

                                }
                            });

                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    //Setting the title manually
                    alert.setTitle("");
                    alert.show();
                }
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent myIntent1 = new Intent(Call.this, MainActivity.class);
                startActivity(myIntent1);
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean validateField(){

        boolean isValidationSuccessful = true;
        String MobilePattern = "[0-9]{10}";

        if(name.getText().toString().trim().equalsIgnoreCase("")){
            Toast.makeText(Call.this,"Name can't be empty",Toast.LENGTH_SHORT).show();
            isValidationSuccessful=false;

        }

        else if(phone.getText().toString().trim().equalsIgnoreCase("")){
            Toast.makeText(Call.this,"Phone number can't be empty",Toast.LENGTH_SHORT).show();
            isValidationSuccessful=false;

        }
        else if(!phone.getText().toString().matches(MobilePattern)) {

            Toast.makeText(getApplicationContext(), "phone number is not valid", Toast.LENGTH_SHORT).show();
            isValidationSuccessful=false;

        }

        return isValidationSuccessful;
    }
}
