/* Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.byteinfotech.corona.coro;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.Spinner;

import com.byteinfotech.corona.coro.permission.PermissionUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCircleClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QuerySnapshot;

import org.imperiumlabs.geofirestore.GeoFirestore;
import org.imperiumlabs.geofirestore.GeoQuery;
import org.imperiumlabs.geofirestore.listeners.GeoQueryEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import static android.app.NotificationManager.IMPORTANCE_DEFAULT;



public class MapActivity extends AppCompatActivity
        implements  OnMapLongClickListener,
        OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener {
    private static final String CHANNEL_ID = "com.byteinfotech.corona.coro";
    private static final LatLng SYDNEY = new LatLng(8.464337, 76.9308838);
    private static final LatLng SYDNEY1 = new LatLng(8.466756, 76.9305378);
    private static final LatLng SYDNEY2 = new LatLng(8.468131, 76.9297278);
    private static final LatLng SYDNEY3 = new LatLng(8.461333, 76.9365678);
    private static final double DEFAULT_RADIUS_METERS = 100;
    private static final double PERSON_RADIUS_METERS = 30;
    private static final int PERSON_COLOR = Color.rgb(0, 0, 255);
    private static final int RED_COLOR = Color.rgb(255, 0, 0);
    // private static final BitmapDescriptor PERSON_bitmap=BitmapDescriptorFactory.fromResource(
    //      R.drawable.person);
    //private static final BitmapDescriptor DEFAULT_bitmap=BitmapDescriptorFactory.defaultMarker(
    // BitmapDescriptorFactory.HUE_RED);
    private static final double RADIUS_OF_EARTH_METERS = 6371009;
    String longitude, latitude, cityName;
    SharedPreferences sharedPreferences;
    double longi, lati;
    double currentLatitude, currentLongitude;
    LocationManager manager;
    FirebaseFirestore db;
    Location mLocation1;
    String latio, longio, edate;
    private boolean mPermissionDenied = false;
    private static final int MAX_WIDTH_PX = 50;
    private static final int MAX_HUE_DEGREES = 360;
    private static final int MAX_ALPHA = 255;
    GeoFirestore geoFirestore;
    LocationManager locationManager;
    String econfirm;
    LocationListener locationListener;
    private static final int PATTERN_DASH_LENGTH_PX = 100;
    private static final int PATTERN_GAP_LENGTH_PX = 200;
    private static final Dot DOT = new Dot();
    private static final Dash DASH = new Dash(PATTERN_DASH_LENGTH_PX);
    private static final Gap GAP = new Gap(PATTERN_GAP_LENGTH_PX);
    private static final List<PatternItem> PATTERN_DOTTED = Arrays.asList(DOT, GAP);
    private static final List<PatternItem> PATTERN_DASHED = Arrays.asList(DASH, GAP);
    private static final List<PatternItem> PATTERN_MIXED = Arrays.asList(DOT, GAP, DOT, DASH, GAP);
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private GoogleMap mMap;
    LatLng loc1;

    private List<DraggableCircle> mCircles = new ArrayList<>(1);
    private List<DraggableCircle1> mCircles1 = new ArrayList<>(1);
    private List<DraggableCircle2> mCircles2 = new ArrayList<>(1);
    BottomNavigationView bottomNavigation;
    private int mFillColorArgb;
    private int mStrokeColorArgb;

    private SeekBar mFillHueBar;
    private SeekBar mFillAlphaBar;
    private SeekBar mStrokeWidthBar;
    private SeekBar mStrokeHueBar;
    private SeekBar mStrokeAlphaBar;
    private Spinner mStrokePatternSpinner;
    private CheckBox mClickabilityCheckbox;
    FusedLocationProviderClient mFusedLocationProviderClient;

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {

    }

    // These are the options for stroke patterns. We use their
    // string resource IDs as identifiers.





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.circle_demo);
       // FirebaseApp.initializeApp(this);

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }


        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }


        bottomNavigation = findViewById(R.id.bottom_navigation);
        Menu menu = bottomNavigation.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);
        sharedPreferences = getApplicationContext().getSharedPreferences("notification", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("notification01", false);
        editor.apply();
        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        Intent myIntent = new Intent(MapActivity.this, MapActivity.class);
                        startActivity(myIntent);
                        finish();
                        break;
                    case R.id.navigation_sms:
                        Intent myIntent1 = new Intent(MapActivity.this, MainActivity.class);
                        startActivity(myIntent1);
                        break;
                    case R.id.navigation_stat:
                        Intent myIntent2 = new Intent(MapActivity.this, Stat.class);
                        startActivity(myIntent2);
                        break;

                }
                return false;
            }
        });




        locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        locationListener = new MyLocationListener();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 10, 1, locationListener);




//        db = FirebaseFirestore.getInstance();
//        db.collection("marker").addSnapshotListener(new EventListener<QuerySnapshot>() {
//            @Override
//            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
//
//                if (e != null) {
//                }
//
//                    for (DocumentChange documentChange : documentSnapshots.getDocumentChanges()) {
//                        Object lat = documentChange.getDocument().getData().get("lat");
//                        if (lat != "") {
//                            if (lat != null) {
//                                latio = lat.toString();
//                            }
//                        }else{latio="0";}
//
//                        Object lon = documentChange.getDocument().getData().get("lon");
//                        if (lon != "") {
//                            if (lon != null) {
//                                longio = lon.toString();
//                            }
//                        }else{longio="0";}
//
//                        Object date = documentChange.getDocument().getData().get("date");
//                        if (date != null) {
//                            edate=date.toString();
//                        }
//
//                        if(latio!="0"&& longio!="0") {
//                            LatLng mar = new LatLng(Double.parseDouble(latio), Double.parseDouble(longio));
//
//                            DraggableCircle circle = new DraggableCircle(mar, DEFAULT_RADIUS_METERS);
//                            mCircles.add(circle);
//
//                            CollectionReference collectionReference=db.collection("geofire");
//                            geoFirestore = new GeoFirestore(collectionReference);
//                            geoFirestore.setLocation(edate, new GeoPoint(Double.parseDouble(latio), Double.parseDouble(longio)));
//                        }
//
//
//                    }
//
//            }
//        });
//
//        db.collection("qpatient").addSnapshotListener(new EventListener<QuerySnapshot>() {
//            @Override
//            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
//
//                if (e != null) {
//                }
//
//                for (DocumentChange documentChange : documentSnapshots.getDocumentChanges()) {
//                    Object lat = documentChange.getDocument().getData().get("lat");
//                    if (lat != "") {
//                        if (lat != null) {
//                            latio = lat.toString();
//                        }
//                    }else{latio="0";}
//
//                    Object lon = documentChange.getDocument().getData().get("lon");
//                    if (lon != "") {
//                        if (lon != null) {
//                            longio = lon.toString();
//                        }
//                    }else{longio="0";}
//
//                    Object date = documentChange.getDocument().getData().get("date");
//                    if (date != null) {
//                        edate=date.toString();
//                    }
//                   Object confirm = documentChange.getDocument().getData().get("confirm");
//                    if (confirm != null) {
//                        if (confirm != null) {
//                            econfirm=confirm.toString();
//                        }
//                    }
//                    if(latio!="0"&& longio!="0") {
//
//                        LatLng mar = new LatLng(Double.parseDouble(latio), Double.parseDouble(longio));
//                        if(econfirm.equals("0")) {
//
//                            DraggableCircle2 circle = new DraggableCircle2(mar, DEFAULT_RADIUS_METERS, R.drawable.person);
//                            mCircles2.add(circle);
//                        }else{DraggableCircle2 circle = new DraggableCircle2(mar, DEFAULT_RADIUS_METERS, R.drawable.redperson);
//                            mCircles2.add(circle);}
//
//
//                    }
//
//
//                }
//
//            }
//        });




    }


    @Override
    public void onMapReady(GoogleMap map) {
        // Override the default content description on the view, for accessibility mode.
        map.setContentDescription("Google");
        mMap = map;
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);
        enableMyLocation();

//        DraggableCircle circle = new DraggableCircle(SYDNEY, DEFAULT_RADIUS_METERS,RED_COLOR);
//        DraggableCircle1 circle1 = new DraggableCircle1(SYDNEY1, DEFAULT_RADIUS_METERS);
//        DraggableCircle2 circle2 = new DraggableCircle2(SYDNEY2, DEFAULT_RADIUS_METERS);
//        DraggableCircle circle3 = new DraggableCircle(SYDNEY3, DEFAULT_RADIUS_METERS,RED_COLOR);
//        mCircles.add(circle);
//        mCircles1.add(circle1);
//        mCircles2.add(circle2);
//        mCircles.add(circle3);

        // Set up the click listener for the circle.

        map.setOnCircleClickListener(new OnCircleClickListener() {
            @Override
            public void onCircleClick(Circle circle) {
                // Flip the red, green and blue components of the circle's stroke color.
                circle.setStrokeColor(circle.getStrokeColor() ^ 0x00ffffff);
            }
        });
        db = FirebaseFirestore.getInstance();
        db.collection("marker").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {

                if (e != null) {
                }

                for (DocumentChange documentChange : documentSnapshots.getDocumentChanges()) {
                    Object lat = documentChange.getDocument().getData().get("lat");
                    if (lat != "") {
                        if (lat != null) {
                            latio = lat.toString();
                        }
                    }else{latio="0";}

                    Object lon = documentChange.getDocument().getData().get("lon");
                    if (lon != "") {
                        if (lon != null) {
                            longio = lon.toString();
                        }
                    }else{longio="0";}

                    Object date = documentChange.getDocument().getData().get("date");
                    if (date != null) {
                        edate=date.toString();
                    }

                    if(!latio.equals("0")&& !longio.equals("0")) {
                        LatLng mar = new LatLng(Double.parseDouble(latio), Double.parseDouble(longio));

                        DraggableCircle circle = new DraggableCircle(mar, DEFAULT_RADIUS_METERS);
                        mCircles.add(circle);

                        CollectionReference collectionReference=db.collection("geofire");
                        geoFirestore = new GeoFirestore(collectionReference);
                        geoFirestore.setLocation(edate, new GeoPoint(Double.parseDouble(latio), Double.parseDouble(longio)));
                    }


                }

            }
        });

        db.collection("qpatient").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {

                if (e != null) {
                }

                for (DocumentChange documentChange : documentSnapshots.getDocumentChanges()) {
                    Object lat = documentChange.getDocument().getData().get("lat");
                    if (lat != "") {
                        if (lat != null) {
                            latio = lat.toString();
                        }
                    }else{latio="0";}

                    Object lon = documentChange.getDocument().getData().get("lon");
                    if (lon != "") {
                        if (lon != null) {
                            longio = lon.toString();
                        }
                    }else{longio="0";}

                    Object date = documentChange.getDocument().getData().get("date");
                    if (date != null) {
                        edate=date.toString();
                    }
                    Object confirm = documentChange.getDocument().getData().get("confirm");
                    if (confirm != "") {
                        if (confirm != null) {
                            econfirm=confirm.toString();
                        }
                    }
                    if(!latio.equals("0")&& !longio.equals("0")) {

                        LatLng mar = new LatLng(Double.parseDouble(latio), Double.parseDouble(longio));
                        if(econfirm.equals("0")) {

                            DraggableCircle2 circle = new DraggableCircle2(mar, DEFAULT_RADIUS_METERS, R.drawable.person,"Quarantined");
                            mCircles2.add(circle);
                            CollectionReference collectionReference=db.collection("geofire1");
                            geoFirestore = new GeoFirestore(collectionReference);
                            geoFirestore.setLocation(edate, new GeoPoint(Double.parseDouble(latio), Double.parseDouble(longio)));
                        }else{DraggableCircle2 circle = new DraggableCircle2(mar, DEFAULT_RADIUS_METERS, R.drawable.redperson,"Confirmed with Corona Virus,Quarantined");
                            mCircles2.add(circle);CollectionReference collectionReference=db.collection("geofire1");
                            geoFirestore = new GeoFirestore(collectionReference);
                            geoFirestore.setLocation(edate, new GeoPoint(Double.parseDouble(latio), Double.parseDouble(longio)));
                        }


                    }


                }

            }
        });

    }

    private void enableMyLocation() {
        // [START maps_check_location_permission]
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
                if (locationManager != null) {
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER, 0, 0, locationListener);
                }
            }
        } else {
            // Permission to access the location is missing. Show rationale and request permission
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        }
        // [END maps_check_location_permission]

        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }

        Location location = null;
        if (lm != null) {
            location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
        double longii = 78.9629;
        if (location != null) {
            longii = location.getLongitude();
        }
        double latii = 20.5937;
        if (location != null) {
            latii = location.getLatitude();
        }

        loc1 = new LatLng(latii, longii);
        sharedPreferences = getApplicationContext().getSharedPreferences("zoom", Context.MODE_PRIVATE);
        final boolean notification = sharedPreferences.getBoolean("map", false);
        if(!notification) {
            // Move the map so that it is centered on the initial circle
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc1, 16.0f));
//        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10);
//        map.animateCamera(cameraUpdate);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("map", true);
            editor.apply();
        }else{            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc1, 16.0f));
        }



    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Permission was denied. Display an error message
            // [START_EXCLUDE]
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
            // [END_EXCLUDE]
        }
    }




    @Override
    public void onMapLongClick(LatLng point) {


    }


    private class DraggableCircle1 {
        private final Marker mCenterMarker;
       // private final Marker mRadiusMarker;
        private final Circle mCircle;
        private double mRadiusMeters;


        public DraggableCircle1(LatLng center, double radiusMeters) {
            mRadiusMeters = radiusMeters;
             mCenterMarker = mMap.addMarker(new MarkerOptions()
                    .position(center)
                    .draggable(true)
                     .icon(BitmapDescriptorFactory.defaultMarker(
                             BitmapDescriptorFactory.HUE_RED)));
              mCircle = mMap.addCircle(new CircleOptions()
                    .center(center)
                    .radius(radiusMeters)
                    .strokeWidth(20)
                      .strokeColor(Color.rgb(255,255,0))
                      .fillColor(Color.rgb(255,255,0))
                    );


        }
        public boolean onMarkerMoved(Marker marker) {

            return false;
        }
    }
    private class DraggableCircle2 {
        private final Marker mCenterMarker;
       // private final Marker mRadiusMarker;
        private final Circle mCircle;
        private double mRadiusMeters;


        public DraggableCircle2(LatLng center, double radiusMeters,int person,String info) {
            mRadiusMeters = radiusMeters;
            mCenterMarker = mMap.addMarker(new MarkerOptions()
                    .position(center)
                    .draggable(true)
                    .icon(BitmapDescriptorFactory.fromResource(person))
                    .title("Info")
                    .snippet(info));
            mCenterMarker.showInfoWindow();
              mCircle = mMap.addCircle(new CircleOptions()
                    .center(center)
                    .radius(30)
                    .strokeWidth(5)
                    .strokeColor(Color.rgb(0, 0, 255))

                    );

        }
        public boolean onMarkerMoved(Marker marker) {
            if (marker.equals(mCenterMarker)) {
                mCircle.setCenter(marker.getPosition());
                return true;
            }

            return false;
        }
    }

    private class DraggableCircle {
        private final Marker mCenterMarker;
        private final Circle mCircle;
        private double mRadiusMeters;


        public DraggableCircle(LatLng center, double radiusMeters ) {
            mRadiusMeters = radiusMeters;
            mCenterMarker = mMap.addMarker(new MarkerOptions()
                    .position(center)
                    .title("Info")
                    .snippet("Visited by Corona infected person")
                    .icon(BitmapDescriptorFactory.defaultMarker(
                            BitmapDescriptorFactory.HUE_RED)));

            mCircle = mMap.addCircle(new CircleOptions()
                    .center(center)
                    .radius(radiusMeters)
                    .strokeWidth(5)
                    .strokeColor(Color.rgb(255,0,0))
                    .fillColor(Color.HSVToColor(100, new float[]{360, 1, 1}))
            );
        }

        public boolean onMarkerMoved(Marker marker) {

            return false;
        }





        public void setClickable(boolean clickable) {
            mCircle.setClickable(clickable);
        }
    }

    public class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location loc) {

            longitude = "Longitude: " + loc.getLongitude();
            Log.v("lon", longitude);
            latitude = "Latitude: " + loc.getLatitude();
            Log.v("lat", latitude);

            longi=loc.getLongitude();
            lati=loc.getLatitude();

            /*------- To get city name from coordinates -------- */
            cityName = null;
            Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
            List<Address> addresses;
            try {
                addresses = gcd.getFromLocation(loc.getLatitude(),
                        loc.getLongitude(), 1);
                if (addresses.size() > 0) {
                    System.out.println(addresses.get(0).getSubLocality());
                    cityName = addresses.get(0).getSubLocality();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            String s = longitude + "\n" + lati + "\n\nMy Current City is: "
                    + cityName;

           // Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();

            if(latitude!=null) {
                db = FirebaseFirestore.getInstance();
                CollectionReference collectionReference=db.collection("geofire");
                geoFirestore = new GeoFirestore(collectionReference);
                GeoQuery geoQuery = geoFirestore.queryAtLocation(new GeoPoint(lati, longi), 0.1);
                geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {

                    @Override
                    public void onKeyExited(String s) {

                        sharedPreferences = getApplicationContext().getSharedPreferences("notification", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("notification01", false);
                        editor.apply();

                    }

                    @Override
                    public void onGeoQueryReady() {

                    }

                    @Override
                    public void onKeyMoved(String s, GeoPoint geoPoint) {

                    }

                    @Override
                    public void onKeyEntered(String s, GeoPoint geoPoint) {
                        // Toast.makeText(getApplicationContext(), "You entered red zone", Toast.LENGTH_SHORT).show();
                        sharedPreferences = getApplicationContext().getSharedPreferences("notification", Context.MODE_PRIVATE);
                        final boolean notification01 = sharedPreferences.getBoolean("notification01", false);

                        if (!notification01) {
                            Intent notificationIntent = new Intent(MapActivity.this, MainActivity.class);

                            TaskStackBuilder stackBuilder = TaskStackBuilder.create(MapActivity.this);
                            stackBuilder.addParentStack(MainActivity.class);
                            stackBuilder.addNextIntent(notificationIntent);

                            PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                            Notification.Builder builder = new Notification.Builder(MapActivity.this);


                            Notification notification = builder.setContentTitle("Warning")
                                    .setContentText("You're entering into Quarantined / Virus infected area..!\nBe careful!")
                                    .setTicker("New Message Alert!")
                                    .setSmallIcon(R.mipmap.ic_launcher_round)
                                    .setContentIntent(pendingIntent).build();

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                builder.setChannelId(CHANNEL_ID);
                            }

                            NotificationManager notificationManager = (NotificationManager) MapActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                NotificationChannel channel = new NotificationChannel(
                                        CHANNEL_ID,
                                        "Fight Corona",
                                        IMPORTANCE_DEFAULT
                                );
                                notificationManager.createNotificationChannel(channel);
                            }

                            notificationManager.notify(0, notification);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putBoolean("notification01", true);
                            editor.apply();
                        }
                    }

                    @Override
                    public void onGeoQueryError(Exception e) {

                    }


                });
                db = FirebaseFirestore.getInstance();
                CollectionReference collectionReference1=db.collection("geofire1");
                geoFirestore = new GeoFirestore(collectionReference1);
                GeoQuery geoQuery1 = geoFirestore.queryAtLocation(new GeoPoint(lati, longi), 0.05);
                geoQuery1.addGeoQueryEventListener(new GeoQueryEventListener() {

                    @Override
                    public void onKeyExited(String s) {

                        sharedPreferences = getApplicationContext().getSharedPreferences("notification", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("notification01", false);
                        editor.apply();

                    }

                    @Override
                    public void onGeoQueryReady() {

                    }

                    @Override
                    public void onKeyMoved(String s, GeoPoint geoPoint) {

                    }

                    @Override
                    public void onKeyEntered(String s, GeoPoint geoPoint) {
                        // Toast.makeText(getApplicationContext(), "You entered red zone", Toast.LENGTH_SHORT).show();
                        sharedPreferences = getApplicationContext().getSharedPreferences("notification", Context.MODE_PRIVATE);
                        final boolean notification01 = sharedPreferences.getBoolean("notification01", false);

                        if (!notification01) {
                            Intent notificationIntent = new Intent(MapActivity.this, MainActivity.class);

                            TaskStackBuilder stackBuilder = TaskStackBuilder.create(MapActivity.this);
                            stackBuilder.addParentStack(MainActivity.class);
                            stackBuilder.addNextIntent(notificationIntent);

                            PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                            Notification.Builder builder = new Notification.Builder(MapActivity.this);


                            Notification notification = builder.setContentTitle("Warning")
                                    .setContentText("You're entering into Quarantined / Virus infected area..!\nBe careful!")
                                    .setTicker("New Message Alert!")
                                    .setSmallIcon(R.mipmap.ic_launcher_round)
                                    .setContentIntent(pendingIntent).build();

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                builder.setChannelId(CHANNEL_ID);
                            }

                            NotificationManager notificationManager = (NotificationManager) MapActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                NotificationChannel channel = new NotificationChannel(
                                        CHANNEL_ID,
                                        "Fight Corona",
                                        IMPORTANCE_DEFAULT
                                );
                                notificationManager.createNotificationChannel(channel);
                            }

                            notificationManager.notify(0, notification);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putBoolean("notification01", true);
                            editor.apply();
                        }
                    }

                    @Override
                    public void onGeoQueryError(Exception e) {

                    }


                });
            }


        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { switch(item.getItemId()) {


        case R.id.about:

            Intent myIntent = new Intent(MapActivity.this, About.class);
            startActivity(myIntent);
            return(true);
        case R.id.exit:
            String shareBody = "Visit www.vastav.live for latest COVID-19 updates.";
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Vastav");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(sharingIntent);
            return(true);
        case R.id.dis:

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.vastav.live/terms"));
            startActivity(browserIntent);
            return(true);

        case R.id.priv:

            Intent browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.vastav.live/privacy-policy.html"));
            startActivity(browserIntent1);
            return(true);

    }
        return(super.onOptionsItemSelected(item));
    }
    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();


        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
        //moveTaskToBack(true);
    }

}