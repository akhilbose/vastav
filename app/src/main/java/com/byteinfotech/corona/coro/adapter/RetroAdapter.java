package com.byteinfotech.corona.coro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.byteinfotech.corona.coro.R;
import java.util.ArrayList;

public class RetroAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ModelListView> dataModelArrayList;

    public RetroAdapter(Context context, ArrayList<ModelListView> dataModelArrayList) {

        this.context = context;
        this.dataModelArrayList = dataModelArrayList;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }
    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getCount() {
        return dataModelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.cell, null, true);

           // holder.iv = (ImageView) convertView.findViewById(R.id.iv);
            holder.tvname = (TextView) convertView.findViewById(R.id.user);
            holder.tvrecover = (TextView) convertView.findViewById(R.id.nrecover);
            holder.tvconfirm = (TextView) convertView.findViewById(R.id.nconfirm);
            holder.tvactive = (TextView) convertView.findViewById(R.id.nactive);
            holder.tvdeath = (TextView) convertView.findViewById(R.id.ndeath);
           // holder.tvcity = (TextView) convertView.findViewById(R.id.city);

            convertView.setTag(holder);
        }else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (ViewHolder)convertView.getTag();
        }

       // Picasso.get().load(dataModelArrayList.get(position).getImgURL()).into(holder.iv);
        holder.tvname.setText(dataModelArrayList.get(position).getName());
        holder.tvrecover.setText(dataModelArrayList.get(position).getRecover());
        holder.tvconfirm.setText(dataModelArrayList.get(position).getConfirm());
        holder.tvactive.setText(dataModelArrayList.get(position).getActive());
        holder.tvdeath.setText(dataModelArrayList.get(position).getDeath());

       // holder.tvcity.setText("City: "+dataModelArrayList.get(position).getCity());

        return convertView;
    }

    private class ViewHolder {

        protected TextView tvname, tvactive,tvconfirm,tvdeath,tvrecover;

    }

}
