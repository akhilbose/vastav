package com.byteinfotech.corona.coro.adapter;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MyInterface {

    String JSONURL = "https://api.covid19india.org/";

    @GET("data.json")
    Call<String> getString();
}
